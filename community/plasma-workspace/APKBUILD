# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=plasma-workspace
pkgver=5.20.0
pkgrel=0
pkgdesc="KDE Plasma Workspace"
arch="all !armhf" # armhf blocked by kirigami2 -> qt5-qtdeclarative
url="https://www.kde.org/workspaces/plasmadesktop/"
license="(GPL-2.0-only OR GPL-3.0-only) AND LGPL-2.1-or-later AND GPL-2.0-or-later AND MIT AND LGPL-2.1-only AND LGPL-2.0-or-later AND (LGPL-2.1-only OR LGPL-3.0-only) AND LGPL-2.0-only"
depends="
	kactivitymanagerd
	kded
	kinit
	kio-extras
	kirigami2
	kquickcharts
	kwin
	milou
	plasma-integration
	qt5-qtquickcontrols
	qt5-qttools
	qtchooser
	tzdata
	"
depends_dev="
	appstream-dev
	baloo-dev
	gpsd-dev
	iso-codes-dev
	kactivities-stats-dev
	kcmutils-dev
	kcoreaddons-dev
	kcrash-dev
	kdbusaddons-dev
	kdeclarative-dev
	kded-dev
	kdelibs4support-dev
	kdesu-dev
	kglobalaccel-dev
	kholidays-dev
	ki18n-dev
	kidletime-dev
	kitemmodels-dev
	kjsembed-dev
	knewstuff-dev
	knotifyconfig-dev
	kpackage-dev
	kpeople-dev
	krunner-dev
	kscreenlocker-dev
	ktexteditor-dev
	ktextwidgets-dev
	kuserfeedback-dev
	kwallet-dev
	kwayland-dev
	kwin-dev
	libkscreen-dev
	libksysguard-dev
	networkmanager-qt-dev
	phonon-dev
	plasma-framework-dev
	prison-dev
	zlib-dev
	"
makedepends="$depends_dev extra-cmake-modules kdoctools-dev libxtst-dev"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/plasma/$pkgver/plasma-workspace-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-libs $pkgname-doc $pkgname-lang"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	# nightcolortest requires running dbus
	# testdesktop, lookandfeel-kcmTest, test_kio_fonts and servicerunnertest are broken
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest -E "(nightcolortest|testdesktop|lookandfeel-kcmTest|test_kio_fonts|servicerunnertest)"
}

package() {
	DESTDIR="$pkgdir" cmake --build build --target install
}
sha512sums="3b49ad852a1c2cfb1b4f304a25423506825b94ca80809e0e9ea55e1efa100b0b5411b3591087c35750f971e78dc640d851a367fae7c33902d02610d21511a613  plasma-workspace-5.20.0.tar.xz"
