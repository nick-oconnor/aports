# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=plasma-thunderbolt
pkgver=5.20.0
pkgrel=0
arch="all !armhf !armv7" # Limited by bolt -> polkit, armhf also limited by qt5-qtdeclarative
url="https://www.kde.org/workspaces/plasmadesktop/"
pkgdesc="Plasma integration for controlling Thunderbolt devices"
license="GPL-2.0-only OR GPL-3.0-only"
depends="bolt"
makedepends="extra-cmake-modules qt5-qtbase-dev qt5-qtdeclarative-dev kcoreaddons-dev kcmutils-dev kdeclarative-dev ki18n-dev kdbusaddons-dev knotifications-dev"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/plasma/$pkgver/plasma-thunderbolt-$pkgver.tar.xz"
subpackages="$pkgname-lang"
options="!check" # Requires running dbus server

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest
}

package() {
	DESTDIR="$pkgdir" cmake --build build --target install
}

sha512sums="c99ff7a606e90b38ef10ce85c4adc375a4425b0d383db58829f8ccf61eeefa290c1f8a2c0b20b10b72af3bf607b4dea2ca0be98f0c6704fcb8e7247f32bfacab  plasma-thunderbolt-5.20.0.tar.xz"
