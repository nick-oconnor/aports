# Contributor: Michael Pirogov <vbnet.ru@gmail.com>
# Contributor: Stuart Cardall <developer at it-offshore dot co.uk>
# Maintainer: Steve McMaster <code@mcmaster.io>
pkgname=suricata
pkgver=5.0.4
pkgrel=0
pkgdesc="High performance Network IDS, IPS and Network Security Monitoring engine"
url="https://suricata-ids.org/"
# mips, s390x lacks rust support
# ppc64le fails to build
arch="all !s390x !ppc64le !mips !mips64"
license="GPL-2.0-only"
makedepends="automake autoconf libtool libhtp-dev>=0.5.25 libcap-ng-dev lz4-dev
	file-dev geoip-dev pcre-dev yaml-dev libpcap-dev hiredis-dev nss-dev
	libnet-dev libnetfilter_queue-dev libnfnetlink-dev jansson-dev nspr-dev
	libnetfilter_log-dev libmaxminddb-dev rust luajit-dev cargo"
depends="py3-yaml python3"
subpackages="$pkgname-doc $pkgname-openrc"
install="$pkgname.post-install"
source="https://www.openinfosecfoundation.org/download/suricata-$pkgver.tar.gz
	$pkgname.confd
	$pkgname.initd
	$pkgname.logrotate
	$pkgname-json-ppc64le.patch
	"

case "$CARCH" in
	x86|x86_64)
		makedepends="$makedepends hyperscan-dev" ;;
esac

prepare() {
	default_prepare
	autoreconf -vif
}

build() {
	HAVE_PYTHON=/usr/bin/python3 ./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var \
		--enable-non-bundled-htp \
		--enable-nfqueue \
		--disable-gccmarch-native \
		--enable-hiredis \
		--enable-geoip \
		--enable-gccprotect \
		--enable-pie \
		--enable-nflog \
		--enable-luajit \
		--enable-rust
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
	make DESTDIR="$pkgdir" install-conf

	cd "$srcdir"
	install -D -m 755 $pkgname.initd "$pkgdir"/etc/init.d/$pkgname
	install -D -m 644 $pkgname.confd "$pkgdir"/etc/conf.d/$pkgname
	install -D -m 644 $pkgname.logrotate "$pkgdir"/etc/logrotate.d/$pkgname

	# install rules
	mkdir -p "$pkgdir"/etc/$pkgname/rules
	install -Dm644 "$builddir"/rules/*.rules "$pkgdir"/etc/$pkgname/rules/
}
sha512sums="e5da14f80b628968e146839b828971e888fd0158b2ecbbcc15c0f42fda2bdcc8ad89632ba05cc45c88d88e537452e77f8e2f3a5e09ecd038d0d38b1a8cf8cea6  suricata-5.0.4.tar.gz
ed7c78a80192f3f3ed433330df323beccb6079b5413289b9e9faa3fceea2c536de93de7372968d8605abd1618d73c9319ee39d86b16eed22e7313c8667252f5d  suricata.confd
258c6d60fc878dc1c7b7bf93cc758080050f591084a1edf7f1aac81ccb523c73615716616fedd0269f9ac5ef2fa7adcb3e2cefd714754bac5571e9806b6781be  suricata.initd
4f76a35bcde78c9860701897fe19bb84cc46bbc429124c4cb2e94cf3330f00ebe8067c0d7f3f83478e9b95323adb947e5081658f455657c4d03c682abe707534  suricata.logrotate
1db0f9941d689a1bf83910692458fa204f684cb966c64c54f4900233bc37eda716cae76efc411faf06e8035982e6d45685b95e187c7f3de4b77add5a40c5ae99  suricata-json-ppc64le.patch"
